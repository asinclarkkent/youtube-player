# YouTube Widget

Display a YouTube player or thumbnail list with a few different options. Read more at [KentHeberling.com](http://web.kentheberling.com/youtube-widget/)