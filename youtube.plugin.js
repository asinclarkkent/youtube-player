function youtubePlugin(name){
	this.name = name;
	this.videoPlayerElement;
	this.videoList;
	this.videosArray = [];
	this.videosDataArray = [];
	this.itemsLoaded = 0;
	this.numberOfVideos = 5;	
	this.buildMethod = "";
	this.playerWidth = "450px";	
	this.playerHeight = "253px";
	this.modalWidth = "640px";
	this.modalHeight = "360px";
	this.firstVid = true;

	this.apiKey = "AIzaSyAbMDpp9SOq7tnGHils1dImX2j1dHfxbLg";

	/* =====================
	Video Player Functions
	===================== */
	
	// Create a youtube player
	this.createVideoPlayer = function(incomingVideoInformation,playerContainer){
		if (incomingVideoInformation.user != null){ // user feed
			this.createFeedVideoPlayer(incomingVideoInformation.user,incomingVideoInformation.numberOfVideos,playerContainer);
		} else if (incomingVideoInformation.playlist != null){ // playlist feed
			this.createPlaylistPlayer(incomingVideoInformation.playlist,incomingVideoInformation.numberOfVideos,playerContainer);
		}  else if (incomingVideoInformation.videos != null){ // list of videos
			this.createVideosPlayer(incomingVideoInformation.videos,incomingVideoInformation.numberOfVideos,playerContainer);
		} 
	}	
	
	// Create a youtube player and thumbnail list given a user feed
	/*this.createFeedVideoPlayer = function(incomingYouTubeUser,incomingNumberOfVideos,playerContainer){
		var youTubeUser = incomingYouTubeUser;
		this.numberOfVideos = incomingNumberOfVideos;
		this.requiresSorting = false;
		this.initVideoPlayerOptions(playerContainer);			
		feedUrl = "http://gdata.youtube.com/feeds/api/users/"+youTubeUser+"/uploads?v=2&alt=json-in-script&format=5&callback="+this.name+".compilePlaylistFeed";		
		this.insertScript(feedUrl);	
	}*/
	
	// Create a youtube player and thumbnail list given a playlist
	this.createPlaylistPlayer = function(incomingPlaylist,incomingNumberOfVideos,playerContainer){
		var playlist = incomingPlaylist;
		this.numberOfVideos = incomingNumberOfVideos;
		this.initVideoPlayerOptions(playerContainer);				
		feedUrl = "https://www.googleapis.com/youtube/v3/playlistItems?playlistId="+playlist+"&maxResults="+incomingNumberOfVideos+"&key="+this.apiKey+"&part=snippet,contentDetails&callback="+this.name+".compilePlaylistFeed";
		this.insertScript(feedUrl);	
	}	

	// Create a youtube player and thumbnail list given a list of videos
	this.createVideosPlayer = function(incomingVideoArray,incomingNumberOfVideos,playerContainer){
		this.videosArray = incomingVideoArray;
		this.numberOfVideos = incomingNumberOfVideos;
		this.initVideoPlayerOptions(playerContainer);	
		feedUrl = "https://www.googleapis.com/youtube/v3/videos?id="+this.videosArray+"&key="+this.apiKey+"&part=snippet,contentDetails&callback="+this.name+".complileVideosFeed";
		this.insertScript(feedUrl);	
	}
	
	
	// Initialize video player options
	this.initVideoPlayerOptions = function(playerContainer){
		this.buildMethod = "videoPlayer";
		this.videoPlayerElement = $(".video_player",playerContainer);
		this.videoList = $(".video_player_thumbs",playerContainer);;
	}
	
	// Build the playlist HTML
	this.buildVideoPlayerHtml = function(){
		var videosHtml = "";
		for (i=0;i<this.videosDataArray.length; i++){
			videosHtml += "<div  id=\"videoDisplay"+this.name+i+"\" class=\"video_entry\" data-videoconfig='{\"id\":\""+this.videosDataArray[i].id+"\", \"width\":\""+this.playerWidth+"\", \"height\":\""+this.playerHeight+"\", \"playerid\":\""+$(this.videoPlayerElement).attr("id")+"\"}'>";
			if (i == (this.videosDataArray.length-1)){ // special class on last item in the list
				videosHtml += '<div class="vb-thumbnail_container last">';
			} else{
				videosHtml += '<div class="vb-thumbnail_container">';
			}
			videosHtml += '<img src="http://img.youtube.com/vi/' + this.videosDataArray[i].id + '/1.jpg" /><span></span>';
			videosHtml += '<p>' + this.videosDataArray[i].title;
			if (this.videosDataArray[i].duration != 0){
				if (this.videosDataArray[i].title.length > 50 ){ // <br> and then time if the title is short
					videosHtml += "<span class='timestamp'>" + this.videosDataArray[i].duration + '</span>';	
				} else {
					videosHtml += "<br /><span class='timestamp noleftmargin'>" + this.videosDataArray[i].duration + '</span>';	
				}
			}
			videosHtml += '</p></div></div>';	
		}	
		$(this.videoList).html(videosHtml);
		
		var parentContainer = $(this.videoPlayerElement).parent();	
		var videosListScroller = $(this.videoList);
		var videosListHeight = $(videosListScroller).height();
		if (videosListHeight > 211){ // Add scroller arrows if playlist is long enough for them
			$(parentContainer).append('<div class="video_scroll_back canClickMe">Back</div><div class="video_scroll_next canClickMe">Next</div>');
			$(".video_scroll_back",parentContainer).fadeTo(0,.5);
		}		
		
		$(videosListScroller).css({"marginTop":"0px"}); // set to 0px so that IE does have it as "auto"
		$(".video_scroll_next",parentContainer).click(function(){
			var clickedNav = $(this);
			if ($(this).hasClass("canClickMe")){ // force slower user clicks. remove class when first click, only add it again when animation is done
				$(this).removeClass("canClickMe");
				if ( (-(videosListHeight-210)) < parseInt($(videosListScroller).css("marginTop")) ){
					$(videosListScroller).animate({"marginTop": (parseInt($(videosListScroller).css("marginTop"))-210) + "px" },350,function(){
						$(clickedNav).addClass("canClickMe");
						if ( (-(videosListHeight-210)) >= parseInt($(videosListScroller).css("marginTop")) ){
							$(clickedNav).fadeTo(100,.5);
						} 
						$(".video_scroll_back",parentContainer).fadeTo(100,1).addClass("canClickMe"); 
					});
				}
			}
		});
		
		$(".video_scroll_back",parentContainer).click(function(){
			var clickedNav = $(this);
			if ($(this).hasClass("canClickMe")){ // force slower user clicks. remove class when first click, only add it again when animation is done
				$(this).removeClass("canClickMe");
				if ( 0 > parseInt($(videosListScroller).css("marginTop")) ){
					$(videosListScroller).animate({"marginTop": (parseInt($(videosListScroller).css("marginTop"))+210) + "px" },350,function(){
						$(clickedNav).addClass("canClickMe");
						if ( 0 <= parseInt($(videosListScroller).css("marginTop")) ){
							$(clickedNav).fadeTo(100,.5);
						} 
						$(".video_scroll_next",parentContainer).fadeTo(100,1).addClass("canClickMe"); 
					});
				}
			}
		});
		
		$(".video_player_thumbs .video_entry").click(function(){
			showVideoInPlayer($(this),true);	
		});	
		showVideoInPlayer($("#videoDisplay"+this.name+"0"), false);
	}
	
	// Show video when user clicks thumbnail
	var showVideoInPlayer = function(videoElement,autoPlay){
		var videoData = $(videoElement).data("videoconfig");
		var videoPlayerElement = $("#"+videoData.playerid + "");
		var videoPlayer = "";
		videoPlayer += "<iframe class='youtube-player' type='text/html' width='" + videoData.width + "' ";
		videoPlayer += "height='" + videoData.height + "' ";
		videoPlayer += "src='http://www.youtube.com/embed/" + videoData.id;
		if (autoPlay){
			videoPlayer += "?autoplay=1'";
		} else {
			videoPlayer += "'";
		}
		 videoPlayer += "frameborder='0'></iframe>"; 
		$(videoPlayerElement).html(videoPlayer);	
	}	
	
	
	
	/* =====================
	Thumnbail List Functions
	===================== */
	
	// Create a youtube thumbnail list
	this.createThumbnailList = function(incomingVideoInformation,listElement,incModalWidth,incModalHeight){
		if (incomingVideoInformation.user != null){ // user feed
			this.createFeedThumbnailList(incomingVideoInformation.user,incomingVideoInformation.numberOfVideos,listElement,incModalWidth,incModalHeight);
		} else if(incomingVideoInformation.playlist != null){ // Playlist
			this.createPlaylistThumbnailList(incomingVideoInformation.playlist,incomingVideoInformation.numberOfVideos,listElement,incModalWidth,incModalHeight);
		} else if (incomingVideoInformation.videos != null){ // List of videos
			this.createVideosThumbnailList(incomingVideoInformation.videos,incomingVideoInformation.numberOfVideos,listElement,incModalWidth,incModalHeight)
		} else if (incomingVideoInformation.manualTitles == true){ // Manual title list
			this.createVideosThumbnailListWithTitles(incomingVideoInformation.list,listElement,incModalWidth,incModalHeight)
		} 

		// Back support for legacy video list include, which was an actual array rather than a text string
		if (Object.prototype.toString.call( incomingVideoInformation ) === '[object Array]'){
			var videosString = "";
			var videosSize = 0;

			for ( var x=0;x <incomingVideoInformation.length;x++){
				videosString += incomingVideoInformation[x] + ",";
				videosSize++;
			}
			this.createVideosThumbnailList(videosString,videosSize,listElement,incModalWidth,incModalHeight);
		}
	}
	
	// Create a thunbnail list from a user feed
	/*this.createFeedThumbnailList = function(incomingYouTubeUser,incomingNumberOfVideos,listElement,incModalWidth,incModalHeight){
		var youTubeUser = incomingYouTubeUser;
		this.numberOfVideos = incomingNumberOfVideos;
		this.initThumbnailOptions(listElement,incModalWidth,incModalHeight);
		this.requiresSorting = false;
		feedUrl = "https://www.googleapis.com/youtube/v3/channels?forUsername="+youTubeUser+"&key="+this.apiKey+"&part=snippet,contentDetails&callback="+this.name+".compilePlaylistFeed";		
		this.insertScript(feedUrl);
	}*/
	
	this.createPlaylistThumbnailList = function(incomingPlaylist,incomingNumberOfVideos,listElement,incModalWidth,incModalHeight){
		var playlist = incomingPlaylist;
		this.initThumbnailOptions(listElement,incModalWidth,incModalHeight);
		this.numberOfVideos = incomingNumberOfVideos;	
		feedUrl = "https://www.googleapis.com/youtube/v3/playlistItems?playlistId="+playlist+"&maxResults="+incomingNumberOfVideos+"&key="+this.apiKey+"&part=snippet,contentDetails&callback="+this.name+".compilePlaylistFeed";
		this.insertScript(feedUrl);	
	}	
	
	// Create a thumbnail list from manual ids
	this.createVideosThumbnailList = function(incomingVideoArray,incomingNumberOfVideos,listElement,incModalWidth,incModalHeight){
		this.videosArray = incomingVideoArray;
		this.initThumbnailOptions(listElement,incModalWidth,incModalHeight);
		this.numberOfVideos = incomingNumberOfVideos;
		feedUrl = "https://www.googleapis.com/youtube/v3/videos?id="+this.videosArray+"&key="+this.apiKey+"&part=snippet,contentDetails&callback="+this.name+".complileVideosFeed";
		this.insertScript(feedUrl);	
	}
	
	// Create a thumbnail list from manual ids and titles
	this.createVideosThumbnailListWithTitles = function(incomingVideoArray,listElement,incModalWidth,incModalHeight){
		this.videosDataArray = incomingVideoArray;
		this.initThumbnailOptions(listElement,incModalWidth,incModalHeight);
		this.buildThumbnailList();
	}	
	
	// Initialize thumbnail list options
	this.initThumbnailOptions = function(listElement,incModalWidth,incModalHeight){
		this.videoList = listElement;
		this.modalWidth = incModalWidth;
		this.modalHeight = incModalHeight;
	}
		
	//  Build a Thumbnail list
	this.buildThumbnailList = function(){
		var videosHtml = "";
		for (i=0; i<this.videosDataArray.length; i++){
			videosHtml += '<div class="video_entry">';
			videosHtml += '<p>' + this.videosDataArray[i].title + '</p>';	
			videosHtml += '<div class="vb-thumbnail_container">';
			videosHtml += "<a class=\"thumbnail_popup\" id=\"videoDisplay"+i+"\"href=\"#videoBox\" data-videoconfig='{\"id\":\""+this.videosDataArray[i].id+"\", \"width\":\""+this.modalWidth+"\", \"height\":\""+this.modalHeight+"\"}'>";
			videosHtml += '<img src="http://img.youtube.com/vi/' + this.videosDataArray[i].id + '/hqdefault.jpg" /><span></span></a></div>    </div>';
		}
		$(this.videoList).html(videosHtml);
		
		$(".thumbnail_popup").click(function(){
			popUpVideo($(this));
		});			
	}
	
	// Shared Pop-Up Function
	popUpVideo = function(videoElement){
		var videoData = $(videoElement).data("videoconfig");
        // create the video player
        var videoPlayer = "";
        videoPlayer += "<iframe class='youtube-player' type='text/html' width='" + videoData.width + "' ";
        videoPlayer += "height='" + videoData.height + "' ";
        videoPlayer += "src='http://www.youtube.com/embed/" + videoData.id + "?autoplay=1' frameborder='0'></iframe>"; 	
		$("#videoBox").html(videoPlayer);	
		$.fancybox(videoPlayer,{
			'transitionIn'  : 'elastic',
			'transitionOut' : 'elastic',
			'speedIn'       : 400, 
			'speedOut'    : 200, 
			'overlayShow'     : true,
			'hideOnOverlayClick' : true,
			'overlayOpacity' : 0.35,
			'overlayColor' : "#000",
			'autoDimensions' : true,
			'autoScale' : true,
			'orig': videoElement
			}
		);
	}	
	
	
	/* =====================
	General Functions
	===================== */	
	this.insertScript = function(feedUrl){
		var scriptTag = document.createElement('script');
		scriptTag.src = feedUrl;
		document.body.appendChild(scriptTag);		
	}
	
	// Compile Video data based on a single video entry
	this.compileVideoData = function (data){
		this.videosDataArray.push({"id":data.items[0].id,"title":data.items[0].snippet.title,"duration":this.formatTime(data.items[0].contentDetails.duration)});
		this.itemsLoaded++; // Track how many items are being loaded and only build HTML when all are done
		if (this.itemsLoaded == this.videosArray.length){
			if (this.buildMethod == "videoPlayer"){
				this.buildVideoPlayerHtml();	
			} else {
				this.buildThumbnailList();
			}	
		}
	}
	
	// Process an incoming multi-entry video feed (V3 API)
	this.compilePlaylistFeed = function (data) {
		var entries = data.items;
		for (vc = 0; vc < this.numberOfVideos; vc++){
			this.videosDataArray.push({"id":entries[vc].contentDetails.videoId,"title":entries[vc].snippet.title,"duration":0});
		}
		if (this.buildMethod == "videoPlayer"){
			this.buildVideoPlayerHtml();	
		} else {
			this.buildThumbnailList();
		}
	}

	// Process an incoming multi-entry video feed
	this.complileVideosFeed = function (data) {
		var entries = data.items;
		for (vc = 0; vc < this.numberOfVideos; vc++){
			this.videosDataArray.push({"id":entries[vc].id,"title":entries[vc].snippet.title,"duration":this.formatTime(entries[vc].contentDetails.duration)});
		}
		if (this.buildMethod == "videoPlayer"){
			this.buildVideoPlayerHtml();	
		} else {
			this.buildThumbnailList();
		}
	}
	
	
	// Video data might not load scripts in original order. Sort array of video data to original order
	this.sortDataToOriginalOrder = function(){
		var placeHolderArray = new Array();
		for (i=0;i<this.videosDataArray.length;i++){
			placeHolderArray.push(this.videosDataArray[i]);
		}
		// clear videoData array
		this.videosDataArray = new Array();
		// sort videoData array to original order
		for (i=0;i<this.videosArray.length;i++){
			for(ii=0;ii<placeHolderArray.length;ii++){
				if (placeHolderArray[ii].id == this.videosArray[i]){
					this.videosDataArray.push(placeHolderArray[ii]);
				}
			}
		}
	}
	
	// Seconds to full time
	this.secondsToTime = function (secs){
		var hours = Math.floor(secs / (60 * 60));
		var divisor_for_minutes = secs % (60 * 60);
		var minutes = Math.floor(divisor_for_minutes / 60);
		var divisor_for_seconds = divisor_for_minutes % 60;
		var seconds = Math.ceil(divisor_for_seconds);
		var stringSeconds = "";
		if (seconds<10){
			stringSeconds = "0" + seconds;
		} else {
			stringSeconds = seconds;
		}
		return minutes + ":" + stringSeconds;
	}	

	// Parse ISO date
	this.parseISO8601Date = function(duration){
		// Crazy RegEx
		var minuteRegEx = /P((([0-9]*\.?[0-9]*)Y)?(([0-9]*\.?[0-9]*)M)?(([0-9]*\.?[0-9]*)W)?(([0-9]*\.?[0-9]*)D)?)?(T(([0-9]*\.?[0-9]*)H)?(([0-9]*\.?[0-9]*)M)?(([0-9]*\.?[0-9]*)S)?)?/
		var match = duration.match(minuteRegEx);
		var totalSeconds = 0;

		// Minutes
		if (parseFloat(match[14])){
			totalSeconds += parseFloat(match[14]) * 60;
		}
		// Seconds
		totalSeconds += parseFloat(match[15]);

		// Output total seconds
		return totalSeconds;
	}

	// Return text string by passing parsed ISO duration into secondsToTime
	this.formatTime = function(duration){
		return this.secondsToTime(this.parseISO8601Date(duration));
	}
}